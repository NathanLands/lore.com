import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Lore - Your NFTs</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Lore
        </h1>

        <p className={styles.description}>
          A beautiful portfolio of all of your NFTs. Coming soon. 
        </p>

      </main>

      <footer className={styles.footer}>
 		Copyright Lore Studios, Inc. 2021 
      </footer>
    </div>
  )
}
